#-------------------------------------------------------------------------------
# Base Image Spec
#-------------------------------------------------------------------------------
ARG BASE_IMAGE=openjdk
ARG BASE_IMAGE_TAG=8-jre-alpine3.7
ARG BASE_IMAGE_NAMESPACE=

FROM mrfroop/openjdk-alpine-gradle AS builder

RUN apk add --no-cache git

RUN git clone https://github.com/owagner/knx2mqtt.git /app

WORKDIR /app
RUN gradle jar

#
# -------
#
FROM ${BASE_IMAGE_NAMESPACE}${BASE_IMAGE}:${BASE_IMAGE_TAG}

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY ./build/pre-build /usr/bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------

WORKDIR /app
COPY --from=builder /app/build/libs/knx2mqtt.jar /app/
VOLUME ["/data"]
CMD java -Dknx2mqtt.knx.nat=NAT \
    -Dknx2mqtt.knx.ip=$knx2mqtt_knx_ip \
    -Dknx2mqtt.mqtt.server=$knx2mqtt_mqtt_server \
    -Dknx2mqtt.mqtt.clientid=$knx2mqtt_mqtt_clientid \
    -Dknx2mqtt.knx.ets5projectfile=/data/$knx2mqtt_knx_ets5projectfile \
    -Dknx2mqtt.mqtt.topic=/knx \
    -jar knx2mqtt.jar

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY ./build/post-build /usr/bin/cross-build-end
RUN [ "cross-build-end" ]

#--------------------------------------------------------------------------------
# Labelling
#--------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION
LABEL de.5square.homesmarthome.build-date=$BUILD_DATE \
      de.5square.homesmarthome.name="homesmarthome/mosquitto" \
      de.5square.homesmarthome.description="MQTT Broker" \
      de.5square.homesmarthome.url="homesmarthome.5square.de" \
      de.5square.homesmarthome.vcs-ref=$VCS_REF \
      de.5square.homesmarthome.vcs-url="$VCS_URL" \
      de.5square.homesmarthome.vendor="5square" \
      de.5square.homesmarthome.version=$VERSION \
      de.5square.homesmarthome.schema-version="1.0"
