docker_run:
	docker run -d --name=mosquitto_test_run homesmarthome/mosquitto:latest
	docker run -d \
	  --name=knx2mqtt_test_run \
		--link mosquitto_test_run:mosquitto \
	  -v $(PWD)/test/env:/env \
	  -v $(PWD)/test:/data \
	  -e knx2mqtt_mqtt_server='tcp://mosquitto:1883' \
	  -e knx2mqtt_knx_ip='10.1.2.3' \
	  -e knx2mqtt_knx_ets5projectfile=hsh-example.knxproj \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep knx2mqtt_test_run

docker_stop:
	docker rm -f knx2mqtt_test_run 2> /dev/null ; true
	docker rm -f mosquitto_test_run 2> /dev/null; true